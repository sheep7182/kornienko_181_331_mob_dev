import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.14
import QtMultimedia 5.14
import QtQuick.Dialogs 1.3
import QtWebView 1.1
import QtWebSockets 1.5
import QtQuick.Window 2.2


Page { //Lab4
    id:lab4
    Rectangle{
        id:fon
        anchors.left: parent.left
        anchors.right: parent.right
        height: parent.height * 0.7
        width: parent.width * 0.9
        anchors.margins: 15
        gradient: Gradient {
            GradientStop { position: 0.0; color: "#8b50a4" }
            GradientStop { position: 1.0; color: "white" }
        }
        opacity: 0.7
        
    }
    ColumnLayout{
        anchors.fill: parent
        
        TextArea {
            id: parskod
            Layout.fillHeight: true
            Layout.fillWidth: true
            // color: "orange"
            Layout.column: 0
            Layout.row: 0
        }
        Button{
            text: "Запрос"
            Layout.column: 0
            Layout.row: 1
            onClicked: {
                signalMakeRequest();
                
            }
            Connections{
                target: httpcontroller // объект - источник сигнала, его необходимо сделать видимым в QML
                function onSignalSendToQML(pString, nString){
                    parskod.append(pString);
                    textFieldNumber.text = nString;
                }
            }
        }
        
        TextField{
            id: textFieldNumber
            readOnly: true
            font{
                family: "Tahoma"
                pixelSize: 20
            }
            width: parent.width*0.25
            implicitHeight: 50
            anchors.margins: 15
            
        }
        
        Text{
            font.pointSize: 16
            text: "Курс доллара:"
        }
        
    }
}
