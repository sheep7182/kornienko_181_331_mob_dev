#include "model.h"

model::model(QObject *parent) : QAbstractListModel(parent)
{

}

// TODO реализация конструктора и get-методов GroupObject простейшая, проработать самостоятельно

void model::addItem(const GroupObject &newItem)
{
    // благодаря beginInsertRows() и endInsertRows() QML реагирует на изменения модели
    beginInsertRows(QModelIndex(),
    rowCount(), //номер строки вставки
    rowCount()); //номер строки, соответствующей концу вставляемого участка
    groups << newItem; //вставка нового элемента данных
    endInsertRows();
}

int model::rowCount(const QModelIndex & parent) const
{
  // метод используется ListView в QML для определения числа элементов
   Q_UNUSED(parent); //скрывает сообщение о том, что параметр не используется
   return groups.count();
}

QVariant model::data(const QModelIndex & index,
                               int role) const
{
  // метод используется в QML для получения значения одного поля под обозначением role одного элемента модели index
   if (index.row() < 0 || (index.row() >= groups.count()))
       return QVariant();

  // TODO дописать выгрузку своих полей под своими обозначениями
   const GroupObject&itemToReturn = groups[index.row()];
   if (role == name)
   return itemToReturn.getname();
   else if (role == description)
   return itemToReturn.getdescription();
   else if (role == is_closed)
   return itemToReturn.getis_closed();
   else if (role == type)
   return itemToReturn.gettype();
   else if (role == photo)
   return itemToReturn.getphoto();
   else if (role == id)
   return itemToReturn.getid();
   else if (role == screen_name)
   return itemToReturn.getscreen_name();
   else if (role == members_count)
   return itemToReturn.getmembers_count();

   return QVariant();
}

QHash<int, QByteArray> model::roleNames() const
{
 // метод используется в QML для сопоставления полей данных со строковыми названиями
  // TODO сопоставить полям данных строковые имена
   QHash<int, QByteArray> roles;
   roles[name] = "name";
   roles[description] = "description";
   roles[is_closed] = "is_closed";
   roles[type] = "type";
   roles[photo] = "photo";
   roles[id] = "id";
   roles[screen_name] = "screen_name";
   roles[members_count] = "members_count";

   return roles;
}

QVariantMap model::get(int idx) const
{
  // метод используется ListView в QML для получения значений полей idx-го элемента модели
   QVariantMap map;
   foreach(int k, roleNames().keys())
   {
       map[roleNames().value(k)] = data(index(idx, 0), k);
   }
   return map;
}

void model::clear()
{
    beginRemoveRows(QModelIndex(), 0, rowCount()-1);
    groups.clear();
    endRemoveRows();
}
GroupObject::GroupObject (const QString p_name,
                           const QString p_description,
                           int p_is_closed,
                           const QString p_type,
                           const QString p_photo,
                           const int p_id,
                           const QString p_screen_name,
                           int p_members_count)

    :m_name(p_name), m_description(p_description), m_is_closed(p_is_closed),
      m_type(p_type), m_photo(p_photo), m_id(p_id),
      m_screen_name (p_screen_name), m_members_count (p_members_count)
{

}

QString GroupObject::getname() const
{
    return m_name;
}

QString GroupObject::getdescription() const
{
    return m_description;
}

int GroupObject::getis_closed() const
{
    return m_is_closed;
}


QString GroupObject::gettype() const
{
    return m_type;
}

QString GroupObject::getphoto() const
{
    return m_photo;
}
int GroupObject::getid() const
{
    return m_id;
}

QString GroupObject::getscreen_name() const
{
    return m_screen_name;
}
int GroupObject::getmembers_count() const
{
    return m_members_count;
}
