import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.14
import QtMultimedia 5.14
import QtQuick.Dialogs 1.3
import QtWebView 1.1
import QtWebSockets 1.5
import QtQuick.Window 2.2

Page { //демонстрация anchors
    id:lecture2
    Button{
        id: button1
        text: "кнопка 1"
        focusPolicy: Qt.StrongFocus
        //font.family: "Arial"
        //font.pixelSize: 30
        anchors.top: parent.top
        anchors.margins: 20
        
        font{
            family: "Arial"
            pixelSize: 30
        }
    }
    
    Button{
        text: "кнопка 2"
        //font.family: "Arial"
        //font.pixelSize: 30
        anchors.right: parent.right
        anchors.left: button1.right
        anchors.top: button1.bottom
        anchors.margins: 20
        height: 50
        
        font{
            family: "Arial"
            pixelSize: 30
        }
    }
    
    Button{
        
        text: "кнопка 3"
        //font.family: "Arial"
        //font.pixelSize: 30
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        anchors.margins: 20
        height: 100
        width: 100
        font{
            family: "Arial"
            pixelSize: 30
        }
    }
}
