import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.14
import QtMultimedia 5.14
import QtQuick.Dialogs 1.3
import QtWebView 1.1
import QtWebSockets 1.5
import QtQuick.Window 2.2

Page { //lab_3
    id:lab3
    Rectangle {
        anchors.fill: parent
        color: "#FFF0F5"
    }
    GridLayout{
        anchors.fill: parent
        anchors.bottomMargin: 5
        columns: 2
        
        Item {
            height: 150
            width: 150
            
            Image {
                id: bug
                source: "qrc:/resource/img/ViberLogo.png"
                sourceSize: Qt.size(parent.width, parent.height)
                smooth: true
                visible: false
            }
            
            HueSaturation {
                anchors.fill: bug
                source: bug
                hue: hue.value
                saturation: 0.5
                lightness: -0.1
            }
        }
        
        ColumnLayout{
            
            Text {
                id: name
                text: qsTr("HueSaturation")
                color: "DarkMagenta"
                font{
                    family: "Tahoma"
                    pixelSize: 20
                }
            }
            Slider{
                id:hue
                value:0
                to:-1
                from:1
            }
        }
        
        Item {
            
            height: 150
            width: 150
            
            
            Image {
                id:  img2
                source:  "qrc:/resource/img/ViberLogo.png"
                sourceSize:  Qt.size( parent.width,  parent.height)
                smooth:  true
                visible:  false
            }
            
            Image {
                id:  mask
                source:  "qrc:/resource/img/play.png"
                sourceSize:  Qt.size( parent.width,  parent.height)
                smooth:  true
                visible:  false
            }
            
            OpacityMask {
                anchors.fill:  img2
                source:  img2
                maskSource:  mask
                invert: invert.checked
            }
        }
        
        ColumnLayout{
            
            Text {
                text: qsTr("OpacityMask.<br> Invert:")
                color: "DarkMagenta"
                font{
                    family: "Tahoma"
                    pixelSize: 20
                }
                
            }
            Switch{
                id:invert
            }
            
        }
        
        Item {
            
            height: 150
            width: 150
            
            Rectangle {
                anchors.fill: parent
                color: "black"
            }
            
            Image {
                id: img_glow
                source: "qrc:/resource/img/ViberLogo.png"
                sourceSize: Qt.size(parent.width, parent.height)
                smooth: true
                visible: false
            }
            
            Glow {
                anchors.fill: img_glow
                spread:spread.value
                radius: radius.value
                samples: samples.value
                color: "white"
                source: img_glow
            }
        }
        
        ColumnLayout{
            Text {
                text: qsTr("Glow")
                color: "DarkMagenta"
                font{
                    family: "Tahoma"
                    pixelSize: 20
                }
            }
            Text {
                text: qsTr("spread:")
                color: "DarkMagenta"
                font{
                    family: "Tahoma"
                }
            }
            Slider{
                id:spread
                to: 0
                from: 1
            }
            Text {
                text: qsTr("Radius:")
                color: "DarkMagenta"
                font{
                    family: "Tahoma"
                }
            }
            Slider{
                id:radius
                to: 50
                from: 0
            }
            
            Text {
                text: qsTr("samples:")
                color: "DarkMagenta"
                font{
                    family: "Tahoma"
                }
            }
            Slider{
                id:samples
                to: 100
                from: 1
            }
        }
    }
}
