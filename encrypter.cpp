#include "encrypter.h"
#include <openssl/conf.h>
#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <openssl/aes.h>
#include <fstream>
#include <QFile>
#include <QTextStream>
#include <QDebug>
#include <QDataStream>

encrypter::encrypter(QObject *parent)
{
    Q_UNUSED(parent);
}

bool encrypter::encrypt(const QString infile, const QString outfile)
{
    unsigned char plaintext[256];
    unsigned char cryptedtext[256];
    EVP_CIPHER_CTX *ctx;
    QByteArray inbites = infile.toLocal8Bit();
    char * inchar = inbites.data();
   // QFile filein(infile);
    //filein.open(QIODevice::ReadOnly |QIODevice::Text);
    QByteArray outbites = outfile.toLocal8Bit();
    char * outchar = outbites.data();
    std::fstream fdef(inchar, std::ios::binary | std::ios::in);
    std::fstream fenc(outchar, std::ios::binary | std::ios::out | std::ios::trunc);
    ctx = EVP_CIPHER_CTX_new();
    EVP_EncryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv);
    int len = 0;
   // filein.read((char*)plaintext, 256);
    fdef.read((char*)plaintext, 256);
    while (fdef.gcount() > 0)
    //while (!fdef.eof())
    //while (!filein.atEnd())
    {
        qDebug() << plaintext;
        EVP_EncryptUpdate(ctx, cryptedtext, &len, plaintext, fdef.gcount());
        fenc.write((char*)cryptedtext, len);
        fdef.read((char*)plaintext, 256);
      //  filein.read((char*)plaintext, 256);
        qDebug() << "конец цикла";
        qDebug() << fdef.gcount();
    }
    EVP_EncryptFinal_ex(ctx, cryptedtext, &len);
    fenc.write((char*)cryptedtext, len);
    fenc.close();
    fdef.close();
    emit toQML71(outfile);
    return true;
}

bool encrypter::decrypt(const QString infile, const QString outfile)
{
    unsigned char cryptedtext[256];
    unsigned char decryptedtext[256];
    EVP_CIPHER_CTX *ctx;
    QByteArray inbites = infile.toLocal8Bit();
    char * inchar = inbites.data();
    QByteArray outbites = outfile.toLocal8Bit();
    char * outchar = outbites.data();
    std::fstream fenc(inchar, std::ios::binary | std::ios::in);
    std::fstream fdec(outchar, std::ios::binary | std::ios::out | std::ios::trunc);
    ctx = EVP_CIPHER_CTX_new();
    EVP_DecryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv);
    int len = 0;
    fenc.read((char*)cryptedtext, 256);
    while (fenc.gcount() > 0)
    {
        qDebug() << "Дешифрую";
        EVP_DecryptUpdate(ctx, decryptedtext, &len, cryptedtext, fenc.gcount());
        fdec.write((char*)decryptedtext, len);
        fenc.read((char*)cryptedtext, 256);
    }
    EVP_DecryptFinal_ex(ctx, decryptedtext, &len);
    fdec.write((char*)decryptedtext, len);
    fdec.close();
    fenc.close();
    emit toQML72(outfile);
    return true;
}
