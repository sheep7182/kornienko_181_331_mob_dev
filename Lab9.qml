import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.14
import QtMultimedia 5.14
import QtQuick.Dialogs 1.3
import QtWebView 1.1
import QtWebSockets 1.5
import QtCharts 2.3

Page{//lab9            
    id:lab9
    //    Binding { target: headerLabel; property: "text"; value: "Графики"; when: swipeView.currentIndex === 7}

    Connections {
        target: httpcontroller

        function onSignalStatisticToQML(less_users, more_users,between) {
            lessChart.value = less_users;
            betweenChart.value = between;
            moreChart.value = more_users;
            lessChart.label = "до 100тыс чел (" + less_users + ")";
            betweenChart.label = "от 100тыс до 1 миллиона чел (" + between + ")";
            moreChart.label = "более 1 миллиона чел (" + more_users + ")";
            chartView.visible = true;
            columnText.visible = false

        }
    }

    ColumnLayout {
        id: columnText
        anchors.fill: parent
        Layout.alignment: Qt.AlignCenter
        Text {
            id: text
            text: "Пройдите авторизацию вк в lab5"
            font.pixelSize: 18
            font.bold: true
            wrapMode: Text.WordWrap
            Layout.fillWidth: true
            horizontalAlignment: Text.AlignHCenter
            color: "#8b50a4"
            font{
                family: "Tahoma"
                pixelSize: 40
            }
        }
    }
    ChartView {
        id: chartView
        title: "Кол-во подписчиков в сообществах вк"
        anchors.fill: parent
        legend.alignment: Qt.AlignBottom
        antialiasing: true

        PieSeries {
            id: pieSeries
            holeSize: 0.3
            PieSlice { id: lessChart; value: 0; color: "#8b50a4" }
            PieSlice { id: betweenChart; value: 0;}
            PieSlice { id: moreChart; value: 0; color: "DarkMagenta"}
        }
        visible: false
    }
}
