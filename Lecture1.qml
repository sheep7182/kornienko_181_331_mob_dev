import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.14
import QtMultimedia 5.14
import QtQuick.Dialogs 1.3
import QtWebView 1.1
import QtWebSockets 1.5
import QtQuick.Window 2.2

Page { //layout
    id:lecture1
    GridLayout {
        anchors.fill: parent
        columns: 3
        Button{
            id: button4
            text: "кнопка 1"
            
            font{
                family: "Arial"
                pixelSize: 30
            }
        }
        
        Button{
            id: button5
            text: "кнопка 2"
            
            font{
                family: "Arial"
                pixelSize: 30
            }
        }
        
        Button{
            id: button6
            text: "кнопка 3"
            
            font{
                family: "Arial"
                pixelSize: 30
            }
        }
        
        Button{
            id: button7
            text: "кнопка 4"
            
            font{
                family: "Arial"
                pixelSize: 30
            }
        }
        
        Button{
            id: button8
            text: "кнопка 5"
            Layout.row: 2
            Layout.column: 2
            //Layout.fillHeight: true
            //Layout.fillWidth: true
            
            Layout.preferredHeight:200
            Layout.preferredWidth: 200
            font{
                family: "Arial"
                pixelSize: 30
            }
        }
        
    }
}
