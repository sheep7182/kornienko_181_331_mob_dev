import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.14
import QtMultimedia 5.14
import QtQuick.Dialogs 1.3
import QtWebView 1.1
import QtWebSockets 1.5
import QtQuick.Window 2.2

Page { //lab_1
    id:lab1
    //header :
    Item {
        id:item1
        width: mainWindow.width
        height: 40
        
        LinearGradient {
            anchors.fill: parent
            gradient: Gradient {
                GradientStop { position: 0.0; color: "#8b50a4" }
            }
        }
        TextArea {
            id: search
            placeholderText: qsTr ("Имя контакта")
            font.family: "Arial"
            color: "white"
            //anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            anchors.left: img.right
            anchors.right: but_lab_1.left
            anchors.leftMargin:10
            anchors.rightMargin:10
        }
        Image {
            id:img
            width: 40
            source: "qrc:/resource/img/ViberLogo.png"
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.left: parent.left
        }
        Button {
            id: but_lab_1
            
            background: Rectangle {
                color: "#8b50a4"
                border.width: 0.5
                //border.color: "white"
                radius: 4
            }
            
            text: "Поиск"
            icon.color: "transparent"
            icon.source: "resource/img/lupa.jpg"
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.right: parent.right
        }
    }
    CheckBox {
        id: check1
        anchors.top: item1.bottom
        anchors.left: parent.left
        text: qsTr("Отображать фото")
        checked: true
    }
    CheckBox {
        id: check2
        anchors.top: check1.bottom
        anchors.left: parent.left
        text: qsTr("Отображать статус")
        checked: true
    }
    CheckBox {
        id:check3
        anchors.top: item1.bottom
        anchors.left: parent.horizontalCenter
        text: qsTr("Вкл уведомления")
        checked: true
    }
    CheckBox {
        id:check4
        anchors.top: check3.bottom
        anchors.left: parent.horizontalCenter
        text: qsTr("Вкл ручной фильтр")
        checked: true
    }
    
    Slider {
        id:slid
        Text {
            text: qsTr("Громкость микрофона")
        }
        anchors.top: check4.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.margins: 20
        from: 1
        value: 10
        to: 100
    }
    Item {
        id:item2
        anchors.top: slid.bottom
        width: mainWindow.width
        height: 40
        
        LinearGradient {
            anchors.fill: parent
            
            start: Qt.point(0, 0)
            end: Qt.point(0, 100)
            gradient: Gradient {
                GradientStop { position: 0.0; color: "#8b50a4" }
                GradientStop { position: 1.0; color: "white" }
            }
        }
        Text {
            anchors.horizontalCenter: parent.horizontalCenter
            text: "Громкость уведомлений"
            font.family: "Helvetica"
            font.pointSize: 15
            color: "white"
        }
    }
    
    Dial {
        id: dial
        anchors.top: item2.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.margins: 20
        value: 50
        width: 187
        height: 152
        to: 99
    }
    
    ProgressBar {
        id:prbar
        anchors.top: dial.bottom
        anchors.bottom : parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.margins: 50
        to: 99
        value: 50
    }
    
    
    footer:
        Button{
        Text {
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            color: "white"
            text: "Сохранить изменения"
            font.family: "Helvetica"
            font.pointSize: 15
        }
        background: Rectangle {
            color: "#8b50a4"
            implicitHeight: 30
            radius: 4
        }
        
    }
    
}
