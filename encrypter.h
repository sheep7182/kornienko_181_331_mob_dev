#ifndef ENCRYPTER_H
#define ENCRYPTER_H
#include <QString>
#include <QObject>
#include <openssl/evp.h>
#include <QDataStream>
class encrypter : public QObject
{
    Q_OBJECT
public:
    explicit encrypter(QObject *parent = nullptr);
public slots:
    bool encrypt(const QString infile, const QString outfile);
    bool decrypt(const QString infile, const QString outfile);
private:
    unsigned char * iv = (unsigned char *)"1234567890123456";
    unsigned char *key = (unsigned char *)"0987654321098765";
signals:
    void toQML71(QString encrypted);
    void toQML72(QString decrypted);
};

#endif // ENCRYPTER_H
