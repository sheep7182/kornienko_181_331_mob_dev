import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.14
import QtMultimedia 5.14
import QtQuick.Dialogs 1.3
import QtWebView 1.1
import QtWebSockets 1.5
import QtQuick.Window 2.2

Page {//lab5
    id:lab5
    GridLayout {
        anchors.fill: parent
        Button {
            id: btAuth
            text: "Авторизация"
            background: Rectangle {
                color: "#8b50a4"
                border.width: 0.5
                //border.color: "white"
                radius: 4
            }
            Layout.alignment: Qt.AlignHCenter
            font.pixelSize: 12
            
            onClicked: {
                browser.visible = true
                browser.url = "https://oauth.vk.com/authorize"
                        + "?client_id=" + "7519032"
                        + "&display=mobile"
                        + "&redirect_uri=https://oauth.vk.com/blanck.html"
                        + "&score=groups"
                        + "&response_type=token"
                        + "&v=5.37"
                        + "&state=123457";
                btAuth.visible = false;
            }
        }
    }
    
    Popup {
        id: popup
        
        width: 200
        height: 200
        x: Math.round((parent.width - width) / 2)
        y: Math.round((parent.height - height) / 2)
        modal: true
        focus: true
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutside
        
        
        ColumnLayout {
            anchors.fill: parent
            
            Text {
                text: "Поздравляю! Вы успешно авторизовались!"
                font.pixelSize: 13
                Layout.preferredWidth: parent.width
                
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                wrapMode: Text.WordWrap
                Layout.fillHeight: true
                
            }
            
            
            Label {
                text: "Access token"
                font.pixelSize: 12
                color: "#4a76a8"
                font.bold: true
                Layout.preferredWidth: parent.width
                
                horizontalAlignment: Label.AlignHCenter
                verticalAlignment: Label.AlignVCenter
            }
            
            Text {
                id: accessToken
                
                Layout.preferredWidth: parent.width
                
                horizontalAlignment: Label.AlignHCenter
                verticalAlignment: Label.AlignVCenter
                
                wrapMode: Text.WrapAnywhere
            }
            
            Button {
                text: "Выход"
                font.pixelSize: 12
                
                Layout.fillWidth: true
                
                onClicked: {
                    Qt.quit()
                    //browser.url = "https://login.vk.com/?act=logout_mobile&hash=3aea8efa8a56fbbaed&reason=tn&_origin=https://vk.com";
                    //popup.close();
                    
                }
            }
        }
    }
  Popup {
        id: popup2

        width: 200
        height: 200
        x: Math.round((parent.width - width) / 2)
        y: Math.round((parent.height - height) / 2)
        modal: true
        focus: true
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutside

        Text {
            text: "Авторизация не прошла"
            font.pixelSize: 13
            Layout.preferredWidth: parent.width

            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            wrapMode: Text.WordWrap
            Layout.fillHeight: true

        }

    }

    
    WebView {
        id: browser
        anchors.fill: parent
        visible: false
        url: ""
        
        onLoadingChanged: {
            
            console.info("*** " + browser.url);
            
            var BrUsl = browser.url;
            var val = httpcontroller.getSomeValueFromCPP(BrUsl);
            if(val !== "") {
                console.log(val)
                accessToken.text = val
                popup.open()
            }
           /* else{
            popup2.open()
            }*/


            if (browser.url == "https://m.vk.com/" || browser.url == "https://m.vk.com/groups") {
                browser.url = "https://oauth.vk.com/authorize"
                        + "?client_id=" + "7519032"
                        + "&display=mobile"
                        + "&redirect_uri=https://oauth.vk.com/blanck.html"
                        + "&score=groups"
                        + "&response_type=token"
                        + "&v=5.37"
                        + "&state=123456";
            }
        }
    }
}
