import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.14
import QtMultimedia 5.14
import QtQuick.Dialogs 1.3
import QtWebView 1.1
import QtWebSockets 1.5
import QtQuick.Window 2.2

Page{//lab10
    id:lab10
    header :
        Item {
        width: mainWindow.width
        height: 40
        
        LinearGradient {
            anchors.fill: parent
            
            start: Qt.point(0, 0)
            end: Qt.point(0, 100)
            gradient: Gradient {
                GradientStop { position: 0.0; color: "#8b50a4" }
                GradientStop { position: 1.0; color: "white" }
            }
        }
        Label {
            text: "Лабораторная работа №10.<br> Простейший чат на WebSocket."
            font.family: "Arial"
            font.pixelSize: 15
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.leftMargin:10
            anchors.rightMargin:10
        }
    }

    ColumnLayout{
        anchors.fill: parent
        
        ListView{
            id: lstMessage
            Layout.fillHeight: true
            Layout.fillWidth: true
            spacing: 10
            verticalLayoutDirection: ListView.BottomToTop
            
            delegate: Item {
                width: lstMessage.width
                height: brd_img.height
                
                
                BorderImage {
                    id: brd_img
                    border {left: 57; top: 45; right: 57; bottom: 45}
                    width: parent.width * 2/3
                    height: txtMessage.contentHeight  + 30 + 20 + 15 + 20
                    source: model_outmessage?"qrc:/resource/img/out_ballon.png":"qrc:/resource/img/in_ballon.png"
                    anchors.left: model_outmessage?undefined:parent.left  //parent.left:undefined
                    anchors.right: model_outmessage?parent.right:undefined //undefined:parent.right
                    
                    
                    ColumnLayout{
                        anchors.fill: parent
                        // anchors.margins: 45
                        anchors.topMargin: 20
                        anchors.bottomMargin: 15
                        anchors.leftMargin: 40
                        anchors.rightMargin: 40
                        TextEdit{ // текст сообщения
                            id: txtMessage
                            text: model_text
                            font.pixelSize: 2.5 * Screen.pixelDensity
                            Layout.fillHeight: true
                            Layout.fillWidth: true
                            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                            readOnly: true
                        }
                        TextEdit{
                            id: txtDate
                            text: model_date
                            font.italic: true
                            Layout.fillWidth: true
                            Layout.preferredHeight: 30
                            horizontalAlignment: Qt.AlignRight
                            wrapMode: Text.WordWrap
                            readOnly: true
                        }
                    }
                    
                }
            }
            
            model: ListModel{
                id: model_messages
                ListElement{
                    model_outmessage: false
                    model_text: "Тест"
                    model_date: "20:00 27 июн 2020г"
                }
                ListElement{
                    model_outmessage: false
                    model_text: "Тест 2"
                    model_date: "20:20 27 июн 2020г"
                }
                
            }
            
        }
        
        Rectangle{
            id:rectaParent
            Layout.fillWidth: true
            Layout.preferredHeight: rect10.height
            
            RowLayout{
                id: row_lay
                Layout.preferredHeight: 50
                Layout.fillWidth: true
                anchors.bottom: parent.bottom
                
                Rectangle{
                    anchors.bottom: parent.bottom
                    id:rect10
                    Layout.preferredHeight: textMessageToSend.contentHeight + 25
                    Layout.preferredWidth: rectaParent.width - batton.width - 10
                    color: "white"
                    radius: 3
                    TextArea{
                        id: textMessageToSend
                        // textFormat: TextEdit.RichText // diego.donate
                        anchors.fill: parent
                        anchors.leftMargin: 5
                        font.pixelSize: 2.5 * Screen.pixelDensity
                        focus: true
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                        placeholderText: "Message"
                        
                    }
                    /*DropArea {
                           anchors.fill: textMessageToSend

                           onEntered: drag.accepted = true
                           onDropped: {
                               var sImageFromRes = "qrc:///resource/img/ViberLogo.png"
                               textMessageToSend.text += "<img src=" + sImageFromRes + " height=25 width=25>"
                           }
                       }*/
                }
                           
                Button{
                    id: batton
                    Layout.preferredHeight: 60
                    icon.color: "transparent"
                    icon.source: "resource/img/Message_(Send).png"
                    Layout.preferredWidth: 60
                    Layout.rightMargin: 5
                    onClicked: {
                        model_messages.insert(0,{"model_outmessage" : true, //Добавляет новый элемент в модель списка
                                                  "model_text": textMessageToSend.text,
                                                  "model_date": new Date().toLocaleString(Qt.locale("ru_RU"))
                                              });

                        ws.sendTextMessage(textMessageToSend.text);
                        textMessageToSend.clear();
                    }
                }
            }
        }
    }
    
    WebSocket {
        id: ws
        active: true
        url: "ws://localhost:8765"
        onTextMessageReceived:{
            console.log("***message : ", message);
            model_messages.insert(0,{"model_outmessage": false,
                                      "model_text": message,
                                      "model_date": new Date().toLocaleString(Qt.locale("ru_RU"))
                                  });

        }
        
        onStatusChanged: {
            switch(status)
            {
            case WebSocket.Connecting:
                console.log("***status : Connecting");
                break;
            case WebSocket.Open:
                console.log("***status : Open ");
                break;
            case WebSocket.Closing:
                console.log("***status : Closing");
                break;
            case WebSocket.Close:
                console.log("***status : Close");
                break;
            case WebSocket.Error:
                console.log("***status : Error");
                console.log("***ErrorString = ", errorString);
                break;
            }
        }
    }
}
