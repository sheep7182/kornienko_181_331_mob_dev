#include <QApplication>
#include "httpcontroller.h"
#include "model.h"
#include "encrypter.h"
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickView>

int main(int argc, char *argv[])
{
    //вызов независимой  функции в составе класса QCoreApplication
    // без создания экземпляра класса (объекта)
    // просто настройка масштабирования экрана
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QApplication app(argc, argv);


    HttpController httpcontroller;
    model GroupModel;
    encrypter cry;
    httpcontroller.GetNetworkValue();

    QQmlApplicationEngine engine; // создание браузерного движка

    QQmlContext *context = engine.rootContext();
    context->setContextProperty("httpcontroller", &httpcontroller); // поместить с++ объект в область видимости движка qml
    context->setContextProperty("GroupModel", &httpcontroller.GroupModel);
    context->setContextProperty("encrypter", &cry);
    httpcontroller.readDB();

    // преобразование пути стартовой страницы из char в QURL
    const QUrl url(QStringLiteral("qrc:/main.qml"));

    // подключение слота, сробатывающего по сигналу objectCreated
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app,
                     [url](QObject *obj, const QUrl &objUrl) // заголовок лямбда-выражения- (функция без названия) вместо отдельного слота
    { // тело лямбда-выражения
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);


    engine.load(url); // загрузка стартовой страницы с адресом url

    QObject * mw = engine.rootObjects().first();
    QObject::connect (mw, SIGNAL(signalMakeRequest()),
                      &httpcontroller, SLOT(GetNetworkValue()));
    QObject::connect(engine.rootObjects().first(), SIGNAL(readDB()),
        &httpcontroller, SLOT(readDB()));

        QObject::connect(engine.rootObjects().first(), SIGNAL(writeDB()),
        &httpcontroller, SLOT(writeDB()));

    return app.exec();//запуск бесконечного цикла обработки сообщений и слотов/сигналов
}
