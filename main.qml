import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.14
import QtMultimedia 5.14
import QtQuick.Dialogs 1.3
import QtWebView 1.1
import QtWebSockets 1.5
import QtQuick.Window 2.2

ApplicationWindow {
    id: mainWindow // если к объекту не планируется обращаться, можно без id
    signal signalMakeRequest
    signal writeDB();
    signal readDB();
    visible: true  //свойство: значение
    width: 480
    height: 640
    title: qsTr("Tabs") // qsTr - функция для переключения языка строки


    Connections{
        target: httpcontroller //откуда
        function onToQML3(str2){
            accessToken.text = str2;
        }
    }
    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: tabBar.currentIndex

        Lecture2 {
            id: lecture2
        }

        Lecture1 {
            id: lecture1
        }


        Lab1 {
            id: lab1
        }

        Lab2 {
            id: lab2
        }


        Lab3 {
            id: lab3
        }


        Lab4 {
            id: lab4
        }

        Lab5 {
            id: lab5
        }


        Lab6 {
            id: lab6
        }

        Lab7 {
            id: lab7
        }
        Lab8 {
            id: lab8
        }
        Lab9 {
            id: lab9
        }
        Lab10 {
            id: lab10
        }

    }



    footer: TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex //

        TabButton {
            text: qsTr("lecture")
        }

        TabButton {
            text: qsTr("lecture")
        }
        TabButton {
            text: qsTr("lab_1")
        }
        TabButton {
            text: qsTr("lab_2")
        }
        TabButton {
            text: qsTr("lab_3")
        }
        TabButton {
            text: qsTr("lab_4")
        }
        TabButton {
            text: qsTr("lab_5")
        }
        TabButton {
            text: qsTr("lab_6")
        }
        TabButton {
            text: qsTr("lab_7")
        }
        TabButton {
            text: qsTr("lab_8")
        }
        TabButton {
            text: qsTr("lab_9")
        }
        TabButton {
            text: qsTr("lab_10")
        }
    }
}

