# список подключённых разделов библиотеки QT
QT += quick
QT += network
QT += charts
QT += sql

# настройки компиляции
CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Refer to the documentation for the
# deprecated API to know how to port your code away from it.

# объйвление переменных и флагов окружения
DEFINES += QT_DEPRECATED_WARNINGS


DEPENDPATH += D:\program\Qt\Tools\OpenSSL\Win_x64\include
INCLUDEPATH += D:\program\Qt\Tools\OpenSSL\Win_x64\include

win32 {
LIBS += D:\program\Qt\Tools\OpenSSL\Win_x64\lib\libcrypto.lib
}
else: android{
LIBS += C:\Qt\Tools\OpenSSL\android_openssl\static\lib\libcrypto.a
}


# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

# раздел файлов исходного кода на С++
SOURCES += \
        HttpController.cpp \
        encrypter.cpp \
        httpcontroller.cpp \
        main.cpp \
        model.cpp

# раздел файлов заголовков C++
# HEADERS

# список файлов, включаемых в раздел ресурсов получаемого исполняемого модуля
RESOURCES += qml.qrc \
    img.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.

# синтаксис "название платформы:" -последующие команды сборки будут
# работать на обозначенной платформе
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    HttpController.h \
    encrypter.h \
    httpcontroller.h \
    model.h
