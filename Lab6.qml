import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.14
import QtMultimedia 5.14
import QtQuick.Dialogs 1.3
import QtWebView 1.1
import QtWebSockets 1.5
import QtQuick.Window 2.2

Page { //lab6
    id:lab6
    property bool menuOpen: false
    property bool auth: false
    header :
        Item {
        width: lab6.width
        height: 40

        LinearGradient {
            anchors.fill: parent

            start: Qt.point(0, 0)
            end: Qt.point(0, 100)
            gradient: Gradient {
                GradientStop { position: 0.0; color: "#8b50a4" }
                GradientStop { position: 1.0; color: "white" }
            }
        }
        Label {
            text: "Лабораторная работа №6.<br> Запросы по REST API. Применение схемы MVC для представления данных"
            font.family: "Arial"
            font.pixelSize: 15
            anchors.top: parent.top
            anchors.left: im.right
            anchors.bottom: parent.bottom
            anchors.right: but_lab_2.left
            anchors.leftMargin:10
            anchors.rightMargin:10
        }
        Image {
            id:im
            width: 40
            source: "qrc:/resource/img/ViberLogo.png"
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.left: parent.left
        }
        ToolButton {
            id:but_lab_2
            anchors.right: parent.right
            text: qsTr("⋮")
            onClicked: menu.open()
            Menu {
                id: menu
                MenuItem {
                    text: "Список"
                    onTriggered: {
                        list.visible = true
                        grid.visible = false
                    }
                }
                MenuItem {
                    text: "Таблица"
                    onTriggered: {
                        list.visible = false
                        grid.visible = true
                    }
                }
                MenuItem {
                    text: "БД"
                    font.pixelSize: 13
                    onTriggered: {
                        httpcontroller.restRequest();
                        httpcontroller.writeDB();
                        httpcontroller.readDB();

                    }
                }
            }
        }
    }
    /*Button {
                            id: buttonvivodne2
                            anchors.horizontalCenter: parent.horizontalCenter
                            visible: true
                            contentItem: Text {
                                horizontalAlignment: Text.AlignHCenter
                                verticalAlignment: Text.AlignVCenter
                                text: qsTr("Обновить")
                                font.pixelSize: 15
                                color: "indigo"
                                font.bold: true
                            }

//                                    background: Rectangle {
//                                        implicitWidth: 80 //абсолютная щирина
//                                        implicitHeight: 35
//                                        color: buttonvivodne2.down ? "#079d4d" : "green" //смена цвета при нажатии
//                                        radius: 10
//                                    }

                            onClicked: {
                                 writeDB();
                                 readDB();
                                }

                            }*/

    ListView {
        model: GroupModel
        id: list
        anchors.fill: parent
        spacing: 5
        delegate: Rectangle {
            id: delegate
            color: "#fff"
            height: Screen.pixelDensity * 20
            RowLayout {
                Image {
                    id: image
                    source: photo
                    smooth: true
                    Item {
                        MouseArea {
                            anchors.fill: parent
                            onClicked: list.currentIndex = indexA
                        }
                        width: image.width
                        height: image.height
                    }
                }
                RowLayout {
                    ColumnLayout {
                        Layout.alignment: Qt.AlignLeft
                        Layout.fillHeight: true
                        Layout.fillWidth: true
                        spacing: 6

                        Label {
                            text: name
                            font.pixelSize: 12
                            font.bold: true

                        }

                        Label {
                            text: screen_name
                            Layout.alignment: Qt.AlignLeft
                            font.pixelSize: 11
                            color: "#9f9f9f"
                        }
                        Label {
                            text: 'id:'+ id
                            Layout.alignment: Qt.AlignLeft
                            font.pixelSize: 11
                            color: "#9f9f9f"
                        }
                        Label {
                            text: type
                            Layout.alignment: Qt.AlignLeft
                            font.pixelSize: 11
                            color: "#9f9f9f"
                        }
                        Label {
                            text: is_closed
                            Layout.alignment: Qt.AlignLeft
                            font.pixelSize: 11
                            color: "#9f9f9f"
                        }
                        Label {
                            text:'Количество участников:'+ members_count
                            Layout.alignment: Qt.AlignLeft
                            font.pixelSize: 11
                            color: "#9f9f9f"
                        }
                    }
                }
            }

        }
    }


    GridView {
        id: grid
        visible: false
        anchors.fill: parent
        cellWidth: lab6.width/2;
        cellHeight: lab6.width/2

        delegate: Rectangle {
            ColumnLayout {
                anchors.fill: parent
                RowLayout {
                    Image {
                        id: imageGrid
                        source: photo
                        smooth: true
                    }

                    ColumnLayout {
                        Label {
                            text: name
                            font.pixelSize: 11
                            font.bold: true
                        }

                        Label {
                            text: screen_name
                            font.pixelSize: 11
                            font.bold: true
                            color: "#9f9f9f"

                        }
                        Label {
                            text: type
                            Layout.alignment: Qt.AlignLeft
                            font.pixelSize: 11
                            color: "#9f9f9f"
                        }

                        Label {
                            text:'id:'+ id
                            Layout.alignment: Qt.AlignLeft
                            font.pixelSize: 11
                            color: "#9f9f9f"
                        }
                        Label {
                            text:'Количество участников:'+ members_count
                            Layout.alignment: Qt.AlignLeft
                            font.pixelSize: 11
                            color: "#9f9f9f"
                        }
                    }

                }
            }

        }
        model: GroupModel
    }
}
