import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.14
import QtMultimedia 5.14
import QtQuick.Dialogs 1.3
import QtWebView 1.1
import QtWebSockets 1.5
import QtQuick.Window 2.2

Page {//lab_2
    id:lab2

    header :
        Item {
        width: mainWindow.width
        height: 40

        LinearGradient {
            anchors.fill: parent

            start: Qt.point(0, 0)
            end: Qt.point(0, 100)
            gradient: Gradient {
                GradientStop { position: 0.0; color: "#8b50a4" }
                GradientStop { position: 1.0; color: "white" }
            }
        }
        Label {
            text: "Лабораторная работа №2.<br> Запись и воспроизведение фото и видео"
            font.family: "Arial"
            font.pixelSize: 15
            anchors.top: parent.top
            anchors.left: im.right
            anchors.bottom: parent.bottom
            anchors.right: kamera_video.left
            anchors.leftMargin:10
            anchors.rightMargin:10
        }
        Image {
            id:im
            width: 40
            source: "qrc:/resource/img/ViberLogo.png"
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.left: parent.left
        }
        RowLayout{
            id:kamera_video
            anchors.top: parent.top
            anchors.right: parent.right
            Text {
                text: camera_switch.checked ? "Камера":"Видео"
                color: "white"
                font{
                    pixelSize: 16
                    family: "Tahoma"
                }
            }

            Switch {
                id:camera_switch
            }
        }
    }

    Rectangle {
        anchors.margins: 10
        anchors.bottomMargin: 70
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        color: "#333333"

        Item{

            visible: camera_switch.checked ? false:true
            anchors.fill: parent

            MediaPlayer {
                id: mediaPlayer
                autoPlay: true
                playlist : Playlist{
                    playbackMode:Playlist.Loop //способ воспроизведения -по циклу
                    PlaylistItem {source: "qrc:/resource/video/sample.mp4" }
                    PlaylistItem {source: "qrc:/resource/video/sample.webm" }

                }
            }

            VideoOutput {
                id: video
                source: mediaPlayer
                anchors.fill: parent
                anchors.margins: 5
                focus: visible
            }

            MouseArea {
                visible: camera_switch.checked ? false:true
                id: area
                anchors.fill: parent
                opacity: 1.0
                onClicked: {

                    timer.running = true
                    if (area.opacity == 0.0) {opacity.running = true} else {
                        no_opacity.running = true

                    }


                }

                Timer {
                    id: timer
                    interval: 7000
                    running: false
                    repeat: false
                    onTriggered: no_opacity.running = true
                }

                SequentialAnimation {
                    id: no_opacity
                    NumberAnimation {
                        target: area
                        properties: "opacity"
                        from: 1.0
                        to: 0.0
                        duration: 100
                    }

                }

                SequentialAnimation {
                    id: opacity
                    NumberAnimation {
                        target: area
                        properties: "opacity"
                        from: 0.0
                        to: 1.0
                        duration: 100
                    }

                }

                Button {
                    id: playButton
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    enabled: mediaPlayer.hasAudio
                    icon.color: "#8b50a4"
                    icon.source: mediaPlayer.playbackState === MediaPlayer.PlayingState ? "qrc:/resource/img/pause.png" : "qrc:/resource/img/play.png"
                    background: Rectangle {
                        opacity: 0.0 // прозрачность
                    }

                    onClicked: mediaPlayer.playbackState === MediaPlayer.PlayingState ? mediaPlayer.pause() : mediaPlayer.play()

                }

                RowLayout{
                    anchors.bottom: parent.bottom
                    anchors.left: parent.left
                    anchors.right: parent.right
                    anchors.margins: 5

                    Button {
                        id: openButton
                        text: qsTr("...")
                        onClicked: fileDialog.open()

                        FileDialog {
                            id: fileDialog
                            folder : shortcuts.movies
                            title: qsTr("Open file")
                            nameFilters: [qsTr("MP3 files (*.mp4)"), qsTr("All files (*.*)")]
                            onAccepted: mediaPlayer.source = fileDialog.fileUrl
                        }
                        background: Rectangle {
                            color: "#8b50a4"
                        }
                    }
                    Button {
                        id: playButton2
                        enabled: mediaPlayer.hasAudio
                        icon.color: "#8b50a4"
                        icon.source: mediaPlayer.playbackState === MediaPlayer.PlayingState ? "qrc:/resource/img/pause.png" : "qrc:/resource/img/play.png"
                        background: Rectangle {
                            opacity: 0.0 // прозрачность
                        }
                        onClicked: mediaPlayer.playbackState === MediaPlayer.PlayingState ? mediaPlayer.pause() : mediaPlayer.play()
                    }

                    Button{
                        id:volumeButton
                        icon.color: "#8b50a4"
                        icon.source: mediaPlayer.muted ? "qrc:/resource/img/35498.png":"qrc:/resource/img/583f1936350d7158b67a7bc9.png"
                        background: Rectangle {
                            color: "#00000000"
                        }
                        onClicked: {
                            if (!mediaPlayer.muted) {mediaPlayer.muted = true} else {mediaPlayer.muted = false}
                        }

                    }


                    Slider {
                        Layout.fillHeight: true
                        Layout.fillWidth: true
                        id: positionSlider
                        to: mediaPlayer.duration

                        property bool sync: false

                        onValueChanged: {
                            if (!sync)
                                mediaPlayer.seek(value)
                        }

                        Connections {
                            target: mediaPlayer
                            onPositionChanged: {
                                positionSlider.sync = true
                                positionSlider.value = mediaPlayer.position
                                positionSlider.sync = false
                            }
                        }
                    }

                    Label {
                        id: positionLabel

                        readonly property int minutes: Math.floor(mediaPlayer.position / 60000)
                        readonly property int seconds: Math.round((mediaPlayer.position % 60000) / 1000)

                        text: Qt.formatTime(new Date(0, 0, 0, 0, minutes, seconds), qsTr("mm:ss"))
                        color: "white"
                    }
                    Button {
                        id: againButton
                        enabled: mediaPlayer.hasAudio
                        icon.color: "#8b50a4"
                        icon.source:"qrc:/resource/img/again.png"
                        background: Rectangle {
                            opacity: 0.0 // прозрачность
                        }
                        onClicked: mediaPlayer.seek(video.position - video.position)
                    }

                }
            }
        }

        Item {
            anchors.fill: parent
            visible: camera_switch.checked ? true:false

            Camera {
                id: camera

                imageProcessing.whiteBalanceMode: CameraImageProcessing.WhiteBalanceFlash

                exposure {
                    exposureCompensation: -1.0
                    exposureMode: Camera.ExposurePortrait
                }

                flash.mode: Camera.FlashRedEyeReduction

                imageCapture {
                    onImageCaptured: {
                    }
                }


            }

            VideoOutput {
                source: camera
                anchors.fill: parent
                focus : visible
            }

            Button{

                anchors.bottom: parent.bottom
                anchors.margins: 25
                anchors.horizontalCenter: parent.horizontalCenter

                Text {
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    text: qsTr("Сделать фото")
                    color: "white"
                    font{
                        family: "Tahoma"
                        pixelSize: 16
                    }
                }

                background: Rectangle {
                    implicitWidth: 250
                    implicitHeight: 45
                    LinearGradient {
                        anchors.fill: parent

                        start: Qt.point(0, 0)
                        end: Qt.point(0, 100)
                        gradient: Gradient {
                            GradientStop { position: 0.0; color: "#8b50a4" }
                            GradientStop { position: 1.0; color: "white" }
                        }
                    }
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        camera.imageCapture.captureToLocation("D:/4semestr/KornienkoEM_181_331_mob_dev/img")
                    }
                }
            }


        }

    }
}
