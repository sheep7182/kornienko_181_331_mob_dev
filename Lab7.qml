
import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.0
import QtQuick.Controls.Styles 1.4
import QtMultimedia 5.14
import QtQuick.Dialogs 1.3
import QtGraphicalEffects 1.14



Page {
    id: lab7
    Connections{
        target: encrypter
        function onToQML71(outfile){
            rct72.visible = true;
            rct73.visible = true;
            txt73.text = "FILEPATH: " + outfile;
            var out = outfile;
            var infile_ToDecrypt = outfile + ".decrypted";
            encrypter.decrypt(out, infile_ToDecrypt);
        }
    }
    Connections{
        target: encrypter
        function onToQML72(outfile){
            rct74.visible = true;
            rct75.visible = true;
            txt75.text = "FILEPATH: " + outfile;
        }
    }
    Button{
        id: btn71
        height: 40;
        font.pointSize: 16;
        anchors.left: parent.left;
        anchors.right: parent.right;
        anchors.top: parent.top;
        anchors.margins: 15;
        onClicked: fd71.open();
        background: Rectangle {

            implicitWidth: 250
            implicitHeight: 45
            opacity: enabled ? 1 : 0.3
            color: "royalblue"
            radius: 15

        }
        Text{
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            color: "white"
            text: qsTr("Выбрать файл")
            font{
                family: "Tahoma"
                pixelSize: 20
            }
        }

    }
    Rectangle{
        id: rct71
        visible: false
        color: "royalblue"
        width: parent.width
        height: parent.height * 0.05
        anchors.top: btn71.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.margins: 15
        Text {
            id: txt71
            wrapMode: TextArea.Wrap
            font.pointSize: 10
            color: "white"
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            anchors.margins: 15
        }
    }
    Rectangle{
        id: rct72
        visible: false
        color: "mediumturquoise"
        width: parent.width
        height: parent.height * 0.05
        anchors.top: rct71.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.margins: 15
        Text {
            id: txt72
            wrapMode: TextArea.Wrap
            font.pointSize: 14
            color: "white"
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            anchors.margins: 15
            text: qsTr("Файл зашифрован")
            font{
                family: "Tahoma"
                pixelSize: 20
            }
        }
    }
    Rectangle{
        id: rct73
        visible: false
        color: "royalblue"
        width: parent.width
        height: parent.height * 0.05
        anchors.top: rct72.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.margins: 15
        Text {
            id: txt73
            wrapMode: TextArea.Wrap
            font.pointSize: 10
            color: "white"
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            anchors.margins: 15
        }
    }
    Rectangle{
        id: rct74
        visible: false
        color: "mediumturquoise"
        width: parent.width
        height: parent.height * 0.05
        anchors.top: rct73.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.margins: 15
        Text {
            id: txt74
            wrapMode: TextArea.Wrap
            font.pointSize: 14
            color: "white"
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            anchors.margins: 15
            text: qsTr("Файл расшифрован")
            font{
                family: "Tahoma"
                pixelSize: 20
            }
        }
    }
    Rectangle{
        id: rct75
        visible: false
        color: "royalblue"
        width: parent.width
        height: parent.height * 0.05
        anchors.top: rct74.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.margins: 15
        Text {
            id: txt75
            wrapMode: TextArea.Wrap
            font.pointSize: 10
            color: "white"
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            anchors.margins: 15
        }
    }
    FileDialog{
        id: fd71
        folder:shortcuts.documents
        onAccepted: {
            rct71.visible = true;
            txt71.text = "FileDialog.fileUrl: " + this.fileUrl;
            var infile_ToEncrypt = fd71.fileUrl.toString().substring(8,fd71.fileUrl.toString().length);
            var infile_ToDecrypt = infile_ToEncrypt + ".encrypted";
            console.log (infile_ToDecrypt);
            encrypter.encrypt(infile_ToEncrypt, infile_ToDecrypt);
        }
    }

}
